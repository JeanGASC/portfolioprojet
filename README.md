# PortFolioProjet


Créer les wireframes, maquettes et pages HTML de votre futur portfolio


# Ressources

https://fontawesome.com/



 # Contexte du projet

Dans le cadre de votre formation de développeur web, vous allez concevoir et coder votre portfolio en page statique :

    Concevoir les wireframes (version desktop et mobile) avec l'outil de votre choix.
    Concevoir les maquettes (version desktop et mobile) avec l'outil de votre choix.
    Traduire la maquette en code HTML et CSS en respectant les consignes suivantes :
    Utiliser les balises sémantiques de l'HTML5 (header, main, footer, nav...);
    Implémenter la bibliothèque Font Awesome pour l'incorporation des différentes icones des réseaux sociaux (gitlab, linkedin);




# Modalités pédagogiques

Le travail est à faire individuellement. Il doit être déposé sur simplonline le mercredi 24 Novembre 2021 avant minuit.


# Modalités d'évaluation

Présentation devant le groupe.
Livrables

Le lien vers votre repo gitlab contenant : les wireframes, les maquettes desktop et mobile, le code HTML et CSS.
